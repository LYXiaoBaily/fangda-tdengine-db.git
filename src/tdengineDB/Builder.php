<?php


namespace TDEngineOrm;
use TDEngineOrm\Expression;

abstract class Builder
{
    // connection对象实例
    protected $connection;
    // 查询对象实例
    protected $query;

    // 数据库表达式
    protected $exp = ['eq' => '=', 'neq' => '<>', 'gt' => '>', 'egt' => '>=', 'lt' => '<', 'elt' => '<=', 'notlike' => 'NOT LIKE', 'not like' => 'NOT LIKE', 'like' => 'LIKE', 'in' => 'IN', 'exp' => 'EXP', 'notin' => 'NOT IN', 'not in' => 'NOT IN', 'between' => 'BETWEEN', 'not between' => 'NOT BETWEEN', 'notbetween' => 'NOT BETWEEN', 'exists' => 'EXISTS', 'notexists' => 'NOT EXISTS', 'not exists' => 'NOT EXISTS', 'null' => 'NULL', 'notnull' => 'NOT NULL', 'not null' => 'NOT NULL', '> time' => '> TIME', '< time' => '< TIME', '>= time' => '>= TIME', '<= time' => '<= TIME', 'between time' => 'BETWEEN TIME', 'not between time' => 'NOT BETWEEN TIME', 'notbetween time' => 'NOT BETWEEN TIME'];
    // SQL表达式
    protected $selectSql    = 'SELECT%DISTINCT% %FIELD% FROM %TABLE%%FORCE%%JOIN%%WHERE%%GROUP%%UNION%%ORDER%%LIMIT%%INTERVAL%%FILL%%LOCK%%COMMENT%';
    protected $insertAutoSql    = '%INSERT% INTO %TABLE% USING %SUPER% TAGS (%TAGS%) (%FIELD%) VALUES (%DATA%) %COMMENT%';
    protected $insertSql    = '%INSERT% INTO %TABLE% USING %SUPER% (%FIELD%) VALUES (%DATA%) %COMMENT%';
    protected $insertAllTableSql = '%TABLE% USING %SUPER% TAGS (%TAGS%) (%FIELD%) VALUES %DATA% %COMMENT%';
    protected $insertAllSql = '%INSERT% INTO %DATA%';
    protected $insertAllSqlString = '%INSERT% INTO %TABLE% USING %SUPER% TAGS (%TAGS%) (%FIELD%) VALUES %DATA% %COMMENT%';
    /**
     * 构造函数
     * @access public
     * @param Connection    $connection 数据库连接对象实例
     * @param Query         $query      数据库查询对象实例
     */
    public function __construct(Connection $connection, Query $query)
    {
        $this->connection = $connection;
        $this->query      = $query;
    }

    /**
     * 获取当前的连接对象实例
     * @access public
     * @return Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * 将SQL语句中的__TABLE_NAME__字符串替换成带前缀的表名（小写）
     * @access protected
     * @param string $sql sql语句
     * @return string
     */
    protected function parseSqlTable($sql)
    {
        return $this->query->parseSqlTable($sql);
    }
    /**
     * 数据分析
     * @access protected
     * @param array     $data 数据
     * @param array     $options 查询参数
     * @return array
     * @throws Exception
     */
    protected function parseData($data, $options)
    {
        if (empty($data)) {
            return [];
        }
        $bind = $this->query->getFieldsBind($options['table']);
        if ('*' == $options['field']) {
            $fields = array_keys($bind);
        } else {
            $fields = $options['field'];
        }
        // 获取绑定信息
        $bind = $this->query->getFieldsBind($options['table']);
        $result = [];

        foreach ($data as $key => $val) {
            if ('*' != $options['field'] && !in_array($key, $fields, true)) {
                continue;
            }

            $item = $this->parseKey($key, $options, true);

            if ($val instanceof Expression) {
                $result[$item] = $val->getValue();
                continue;
            } elseif (is_object($val) && method_exists($val, '__toString')) {
                // 对象数据写入
                $val = $val->__toString();

            }

            if (false === strpos($key, '.') && !in_array($key, $fields, true)) {

                $result[$item] = $val;

//                if ($options['strict']) {
//                    throw new Exception('fields not exists:[' . $key . ']');
//                }
            } elseif (is_null($val)) {
                $result[$item] = 'NULL';
            } elseif (is_array($val) && !empty($val)) {
                switch (strtolower($val[0])) {
                    case 'inc':
                        $result[$item] = $item . '+' . floatval($val[1]);
                        break;
                    case 'dec':
                        $result[$item] = $item . '-' . floatval($val[1]);
                        break;
                    case 'exp':
                        throw new Exception('not support data:[' . $val[0] . ']');
                }
            } elseif (is_scalar($val)) {

                // 过滤非标量数据
//                if (0 === strpos($val, ':') && $this->query->isBind(substr($val, 1))) {
//                    $result[$item] = $val;
//
//                } else {
//                    $key = str_replace('.', '_', $key);
//                    $this->query->bind('data__' . $key, $val, isset($bind[$key]) ? $bind[$key] : 0);
//                    $result[$item] = ':data__' . $key;
//                }
                $result[$item] = $val;
            }
        }

        return $result;
    }
    /**
     * 字段名分析
     * @access protected
     * @param string $key
     * @param array  $options
     * @return string
     */
    protected function parseKey($key, $options = [], $strict = false)
    {
        return $key;
    }
    /**
     * value分析
     * @access protected
     * @param mixed     $value
     * @param string    $field
     * @return string|array
     */
    protected function parseValue($value, $field = '')
    {
        if (is_string($value)) {

            $value = strpos($value, ':') === 0 && $this->query->isBind(substr($value, 1)) ? $value : $value;
        } elseif (is_array($value)) {
            $value = array_map([$this, 'parseValue'], $value);
        } elseif (is_bool($value)) {
            $value = $value ? '1' : '0';
        } elseif (is_null($value)) {
            $value = 'null';
        }

        return $value;
    }
    /**
     * table分析
     * @access protected
     * @param mixed $tables
     * @param array $options
     * @return string
     */
    protected function parseTable($tables, $options = [])
    {
        $item = [];
        foreach ((array) $tables as $key => $table) {
            if (!is_numeric($key)) {
                $key    = $this->parseSqlTable($key);
                $item[] = $this->parseKey($key) . ' ' . (isset($options['alias'][$table]) ? $this->parseKey($options['alias'][$table]) : $this->parseKey($table));
            } else {
                $table = $this->parseSqlTable($table);
                if (isset($options['alias'][$table])) {
                    $item[] = $this->parseKey($table) . ' ' . $this->parseKey($options['alias'][$table]);
                } else {
                    $item[] = $this->parseKey($table);
                }
            }
        }
        return implode(',', $item);
    }

    /**
     * field分析
     * @access protected
     * @param mixed     $fields
     * @param array     $options
     * @return string
     */
    protected function parseField($fields, $options = [])
    {
        if ('*' == $fields || empty($fields)) {
            $fieldsStr = ' *';
        } elseif (is_array($fields)) {
            // 支持 'field1'=>'field2' 这样的字段别名定义
            $array = [];
            foreach ($fields as $key => $field) {
                if ($field instanceof Expression) {
                    $array[] = $field->getValue();
                } elseif (!is_numeric($key)) {
                    $array[] = $this->parseKey($key, $options) . ' AS ' . $this->parseKey($field, $options, true);
                } else {
                    $array[] = $this->parseKey($field, $options);
                }
            }
            $fieldsStr = implode(',', $array);
        }
        return $fieldsStr;
    }

    /**
     *
     * @DESC:using 分析
     * @email:710388898@qq.com
     * @param $using
     * @param array $option
     * @author: 杨同师
     * @Time: 2022/9/13   11:37
     *
     */
    protected function parseUsing($using,$option=[]){
        if (empty($using)) {
            $usingStr = '';
        }
        $usingStr=$option['using'];
        return $usingStr;
    }

    /**
     *
     * @DESC:tags 分析
     * @email:710388898@qq.com
     * @param $tags
     * @param array $option
     * @return array|string
     * @author: 杨同师
     * @Time: 2022/9/13   11:44
     *
     */
    protected function parseTags($tags,$option=[]){
        if(empty($tags)){
            $tagsStr='';
        }
        $tagsStr=implode(',',array_values($tags));

        return $tagsStr;
    }

    /**
     * join分析
     * @access protected
     * @param array $join
     * @param array $options 查询条件
     * @return string
     */
    protected function parseJoin($join, $options = [])
    {
        $joinStr = '';
        if (!empty($join)) {
            foreach ($join as $item) {
                list($table, $type, $on) = $item;
                $condition               = [];
                foreach ((array) $on as $val) {
                    if ($val instanceof Expression) {
                        $condition[] = $val->getValue();
                    } elseif (strpos($val, '=')) {
                        list($val1, $val2) = explode('=', $val, 2);
                        $condition[]       = $this->parseKey($val1, $options) . '=' . $this->parseKey($val2, $options);
                    } else {
                        $condition[] = $val;
                    }
                }

                $table = $this->parseTable($table, $options);
                $joinStr .= ' ' . $type . ' JOIN ' . $table . ' ON ' . implode(' AND ', $condition);
            }
        }
        return $joinStr;
    }
    /**
     * where分析
     * @access protected
     * @param mixed $where   查询条件
     * @param array $options 查询参数
     * @return string
     */
    protected function parseWhere($where, $options)
    {
        $whereStr = $this->buildWhere($where, $options);

        if (!empty($options['soft_delete'])) {

            // 附加软删除条件
            list($field, $condition) = $options['soft_delete'];

            $binds    = $this->query->getFieldsBind($options['table']);
            $whereStr = $whereStr ? '( ' . $whereStr . ' ) AND ' : '';
            $whereStr = $whereStr . $this->parseWhereItem($field, $condition, '', $options, $binds);
        }

        return empty($whereStr) ? '' : ' WHERE ' . $whereStr;
    }
    /**
     * 生成查询条件SQL
     * @access public
     * @param mixed     $where
     * @param array     $options
     * @return string
     */
    public function buildWhere($where, $options)
    {
        if (empty($where)) {
            $where = [];
        }

        if ($where instanceof Query) {
            return $this->buildWhere($where->getOptions('where'), $options);
        }

        $whereStr = '';
        $binds    = $this->query->getFieldsBind($options['table']);
        foreach ($where as $key => $val) {
            $str = [];
            foreach ($val as $field => $value) {
                if ($value instanceof Expression) {
                    $str[] = ' ' . $key . ' ( ' . $value->getValue() . ' )';
                } elseif ($value instanceof \Closure) {
                    // 使用闭包查询
                    $query = new Query($this->connection);
                    call_user_func_array($value, [ & $query]);
                    $whereClause = $this->buildWhere($query->getOptions('where'), $options);
                    if (!empty($whereClause)) {
                        $str[] = ' ' . $key . ' ( ' . $whereClause . ' )';
                    }
                } elseif (strpos($field, '|')) {
                    // 不同字段使用相同查询条件（OR）
                    $array = explode('|', $field);
                    $item  = [];
                    foreach ($array as $k) {
                        $item[] = $this->parseWhereItem($k, $value, '', $options, $binds);
                    }
                    $str[] = ' ' . $key . ' ( ' . implode(' OR ', $item) . ' )';
                } elseif (strpos($field, '&')) {
                    // 不同字段使用相同查询条件（AND）
                    $array = explode('&', $field);
                    $item  = [];
                    foreach ($array as $k) {
                        $item[] = $this->parseWhereItem($k, $value, '', $options, $binds);
                    }
                    $str[] = ' ' . $key . ' ( ' . implode(' AND ', $item) . ' )';
                } else {

                    // 对字段使用表达式查询
                    $field = is_string($field) ? $field : '';
                    $str[] = ' ' . $key . ' ' . $this->parseWhereItem($field, $value, $key, $options, $binds);

                }
            }

            $whereStr .= empty($whereStr) ? substr(implode(' ', $str), strlen($key) + 1) : implode(' ', $str);
        }
        return $whereStr;
    }
    // where子单元分析
    protected function parseWhereItem($field, $val, $rule = '', $options = [], $binds = [], $bindName = null)
    {
        // 字段分析
        $key = $field ? $this->parseKey($field, $options, true) : '';

        // 查询规则和条件
        if (!is_array($val)) {
            $val = is_null($val) ? ['null', ''] : ['=', $val];
        }
        list($exp, $value) = $val;

        // 对一个字段使用多个查询条件
        if (is_array($exp)) {
            $item = array_pop($val);

            // 传入 or 或者 and
            if (is_string($item) && in_array($item, ['AND', 'and', 'OR', 'or'])) {

                $rule = $item;
            } else {

                array_push($val, $item);
            }
            foreach ($val as $k => $item) {
                $bindName = 'where_' . str_replace('.', '_', $field) . '_' . $k;
                $str[]    = $this->parseWhereItem($field, $item, $rule, $options, $binds, $bindName);
            }

            return '( ' . implode(' ' . $rule . ' ', $str) . ' )';
        }

        // 检测操作符
        if (!in_array($exp, $this->exp)) {
            $exp = strtolower($exp);
            if (isset($this->exp[$exp])) {
                $exp = $this->exp[$exp];
            } else {
                throw new Exception('where express error:' . $exp);
            }
        }
        $bindName = $bindName ?: 'where_' . $rule . '_' . str_replace(['.', '-'], '_', $field);
       // $bindName=$bindName ? :$val;
        if (preg_match('/\W/', $bindName)) {
            // 处理带非单词字符的字段名
            $bindName = md5($bindName);
        }

        if ($value instanceof Expression) {

        } elseif (is_object($value) && method_exists($value, '__toString')) {
            // 对象数据写入
            $value = $value->__toString();
        }

        $bindType = isset($binds[$field]) ? $binds[$field] : 4;
        if (is_scalar($value) && array_key_exists($field, $binds) && !in_array($exp, ['EXP', 'NOT NULL', 'NULL', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN']) && strpos($exp, 'TIME') === false) {
            if (strpos($value, ':') !== 0 || !$this->query->isBind(substr($value, 1))) {
                if ($this->query->isBind($bindName)) {
                    $bindName .= '_' . str_replace('.', '_', uniqid('', true));
                }
                $this->query->bind($bindName, $value, $bindType);
                if($bindType==4||$bindType==1){
                    $value="'".$val[1]."'";
                }else{
                    $value=$val[1];
                }
                //$value = ':' . $bindName;
            }
        }
        $whereStr = '';
        if (in_array($exp, ['=', '<>', '>', '>=', '<', '<='])) {

            // 比较运算
            if ($value instanceof \Closure) {
                $whereStr .= $key . ' ' . $exp . ' ' . $this->parseClosure($value);
            } else {
                $whereStr .= $key . ' ' . $exp . ' ' . $this->parseValue($value, $field);
            }

        } elseif ('LIKE' == $exp || 'NOT LIKE' == $exp) {
            // 模糊匹配
            if (is_array($value)) {
                foreach ($value as $item) {
                    $array[] = $key . ' ' . $exp . ' ' . $this->parseValue($item, $field);
                }
                $logic = isset($val[2]) ? $val[2] : 'AND';
                $whereStr .= '(' . implode($array, ' ' . strtoupper($logic) . ' ') . ')';
            } else {
                $whereStr .= $key . ' ' . $exp . ' ' . $this->parseValue($value, $field);
            }
        } elseif ('EXP' == $exp) {
            // 表达式查询
            if ($value instanceof Expression) {
                $whereStr .= '( ' . $key . ' ' . $value->getValue() . ' )';
            } else {
                throw new Exception('where express error:' . $exp);
            }
        } elseif (in_array($exp, ['NOT NULL', 'NULL'])) {
            // NULL 查询
            $whereStr .= $key . ' IS ' . $exp;
        } elseif (in_array($exp, ['NOT IN', 'IN'])) {
            // IN 查询
            if ($value instanceof \Closure) {
                $whereStr .= $key . ' ' . $exp . ' ' . $this->parseClosure($value);
            } else {
                $value = array_unique(is_array($value) ? $value : explode(',', $value));
                if (array_key_exists($field, $binds)) {
                    $bind  = [];
                    $array = [];
                    $i     = 0;
                    foreach ($value as $v) {
                        $i++;
                        if ($this->query->isBind($bindName . '_in_' . $i)) {
                            $bindKey = $bindName . '_in_' . uniqid() . '_' . $i;
                        } else {
                            $bindKey = $bindName . '_in_' . $i;
                        }
                        $bind[$bindKey] = [$v, $bindType];
                        $array[]        = "'".$v."'";
                    }
                    $this->query->bind($bind);
                    $zone = implode(',', $array);

                } else {
                    $zone = implode(',', $this->parseValue($value, $field));
                }
                $whereStr .= $key . ' ' . $exp . ' (' . (empty($zone) ? "''" : $zone) . ')';
            }
        } elseif (in_array($exp, ['NOT BETWEEN', 'BETWEEN'])) {
            // BETWEEN 查询
            $bindValue1='';
            $bindValue2='';
            $data = is_array($value) ? $value : explode(',', $value);
            if (array_key_exists($field, $binds)) {
                if ($this->query->isBind($bindName . '_between_1')) {
                    $bindKey1 = $bindName . '_between_1' . uniqid();
                    $bindKey2 = $bindName . '_between_2' . uniqid();
                } else {
                    $bindKey1 = $bindName . '_between_1';
                    $bindKey2 = $bindName . '_between_2';
                }
                $bind = [
                    $bindKey1 => [$data[0], $bindType],
                    $bindKey2 => [$data[1], $bindType],
                ];
                $this->query->bind($bind);
                if($bindType==1){
                    $bindValue1="'".$data[0]."'";
                    $bindValue2="'".$data[1]."'";
                }
                $between = $bindValue1. ' AND ' .$bindValue2;
            } else {
                $between = $this->parseValue($data[0], $field) . ' AND ' . $this->parseValue($data[1], $field);
            }
            $whereStr .= $key . ' ' . $exp . ' ' . $between;
        } elseif (in_array($exp, ['NOT EXISTS', 'EXISTS'])) {
            // EXISTS 查询
            if ($value instanceof \Closure) {
                $whereStr .= $exp . ' ' . $this->parseClosure($value);
            } else {
                $whereStr .= $exp . ' (' . $value . ')';
            }
        } elseif (in_array($exp, ['< TIME', '> TIME', '<= TIME', '>= TIME'])) {
            $whereStr .= $key . ' ' . substr($exp, 0, 2) . ' ' . $this->parseDateTime($value, $field, $options, $bindName, $bindType);
        } elseif (in_array($exp, ['BETWEEN TIME', 'NOT BETWEEN TIME'])) {
            if (is_string($value)) {
                $value = explode(',', $value);
            }
            $whereStr .= $key . ' ' . substr($exp, 0, -4) . $this->parseDateTime($value[0], $field, $options, $bindName . '_between_1', $bindType) . ' AND ' . $this->parseDateTime($value[1], $field, $options, $bindName . '_between_2', $bindType);
        }
        return $whereStr;
    }

    // 执行闭包子查询
    protected function parseClosure($call, $show = true)
    {
        $query = new Query($this->connection);
        call_user_func_array($call, [ & $query]);
        return $query->buildSql($show);
    }
    /**
     * 日期时间条件解析
     * @access protected
     * @param string    $value
     * @param string    $key
     * @param array     $options
     * @param string    $bindName
     * @param integer   $bindType
     * @return string
     */
    protected function parseDateTime($value, $key, $options = [], $bindName = null, $bindType = null)
    {
        // 获取时间字段类型
        if (strpos($key, '.')) {
            list($table, $key) = explode('.', $key);
            if (isset($options['alias']) && $pos = array_search($table, $options['alias'])) {
                $table = $pos;
            }
        } else {
            $table = $options['table'];
        }
        $type = $this->query->getTableInfo($table, 'type');
        if (isset($type[$key])) {
            $info = $type[$key];
        }
        if (isset($info)) {
            if (is_string($value)) {
                $value = strtotime($value) ?: $value;
            }

            if (preg_match('/(datetime|timestamp)/is', $info)) {
                // 日期及时间戳类型
                $value = date('Y-m-d H:i:s', $value);
            } elseif (preg_match('/(date)/is', $info)) {
                // 日期及时间戳类型
                $value = date('Y-m-d', $value);
            }
        }
        $bindName = $bindName ?: $key;

        if ($this->query->isBind($bindName)) {
            $bindName .= '_' . str_replace('.', '_', uniqid('', true));
        }

        $this->query->bind($bindName, $value, $bindType);
        return ':' . $bindName;
    }
    /**
     * group分析
     * @access protected
     * @param mixed $group
     * @return string
     */
    protected function parseGroup($group)
    {
        return !empty($group) ? ' GROUP BY ' . $this->parseKey($group) : '';
    }
    /**
     * distinct分析
     * @access protected
     * @param mixed $distinct
     * @return string
     */
    protected function parseDistinct($distinct)
    {
        return !empty($distinct) ? ' DISTINCT ' : '';
    }

    /**
     * order分析
     * @access protected
     * @param mixed $order
     * @param array $options 查询条件
     * @return string
     */
    protected function parseOrder($order, $options = [])
    {
        if (empty($order)) {
            return '';
        }

        $array = [];
        foreach ($order as $key => $val) {
            if ($val instanceof Expression) {
                $array[] = $val->getValue();
            } elseif ('[rand]' == $val) {
                $array[] = $this->parseRand();
            } else {
                if (is_numeric($key)) {
                    list($key, $sort) = explode(' ', strpos($val, ' ') ? $val : $val . ' ');
                } else {
                    $sort = $val;
                }
                $sort    = strtoupper($sort);
                $sort    = in_array($sort, ['ASC', 'DESC'], true) ? ' ' . $sort : '';
                $array[] = $this->parseKey($key, $options, true) . $sort;
            }
        }
        $order = implode(',', $array);

        return !empty($order) ? ' ORDER BY ' . $order : '';
    }
    /**
     * limit分析
     * @access protected
     * @param mixed $limit
     * @return string
     */
    protected function parseLimit($limit)
    {
        return (!empty($limit) && false === strpos($limit, '(')) ? ' LIMIT ' . $limit . ' ' : '';
    }
    /**
     * limit分析
     * @access protected
     * @param mixed $limit
     * @return string
     */
    protected function parseInterval($interval,$options)
    {
//        if (empty($interval)) {
//            $intervalStr = '';
//        }
//        $intervalStr=$options['interval'];
//        return $intervalStr;
        return !empty($interval) ? ' INTERVAL('. $interval.')' : '';
    }
    /**
     * limit分析
     * @access protected
     * @param mixed $limit
     * @return string
     */
    protected function parseFillval($interval,$options)
    {
//        if (empty($interval)) {
//            $intervalStr = '';
//        }
//        $intervalStr=$options['interval'];
//        return $intervalStr;
        return !empty($interval) ? ' FILL('. $interval.')' : '';
    }
    /**
     * 生成查询SQL
     * @access public
     * @param array $options 表达式
     * @return string
     */
    public function select($options = [])
    {

        $sql = str_replace(
            ['%TABLE%', '%DISTINCT%', '%FIELD%', '%JOIN%', '%WHERE%', '%GROUP%',  '%ORDER%', '%LIMIT%', '%INTERVAL%','%FILL%','%UNION%', '%LOCK%', '%COMMENT%', '%FORCE%'],
            [
                $this->parseTable($options['table'], $options),
                $this->parseDistinct($options['distinct']),
                $this->parseField($options['field'], $options),
                $this->parseJoin($options['join'], $options),
                $this->parseWhere($options['where'], $options),
                $this->parseGroup($options['group']),
                $this->parseOrder($options['order'], $options),
                $this->parseLimit($options['limit']),
                $this->parseInterval($options['interval'],$options),
                $this->parseFillval($options['fill'],$options),
//                $this->parseUnion($options['union']),
//                $this->parseLock($options['lock']),
//                $this->parseComment($options['comment']),
//                $this->parseForce($options['force']),
            ], $this->selectSql);
       
        return $sql;
    }
    /**
     * 生成insert SQL
     * @access public
     * @param array     $data 数据
     * @param array     $options 表达式
     * @param bool      $replace 是否replace
     * @return string
     */
    public function insert(array $data, $options = [], $replace = false,$auto=false)
    {

        // 分析并处理数据
        $data = $this->parseData($data, $options);

        if (empty($data)) {
            return 0;
        }
        $fields = array_keys($data);
        $values = array_values($data);

        if($auto){
            $sql = str_replace(
                ['%INSERT%', '%TABLE%','%SUPER%','%TAGS%', '%FIELD%', '%DATA%', '%COMMENT%'],
                [
                    $replace ? 'REPLACE' : 'INSERT',
                    $this->parseTable($options['table'], $options),
                    $this->parseUsing($options['using'], $options),
                    $this->parseTags($options['tags'], $options),
                    implode(',', $fields),
                    implode(',', $values),
                ], $this->insertAutoSql);
        }else{
            $sql = str_replace(
                ['%INSERT%', '%TABLE%','%SUPER%', '%FIELD%', '%DATA%', '%COMMENT%'],
                [
                    $replace ? 'REPLACE' : 'INSERT',
                    $this->parseTable($options['table'], $options),
                    $this->parseUsing($options['using'], $options),
                    implode(' , ', $fields),
                    implode(' , ', $values),
                ], $this->insertSql);
        }

        return $sql;
    }
    /**
     * 生成insertall SQL
     * @access public
     * @param array     $dataSet 数据集
     * @param array     $options 表达式
     * @param bool      $replace 是否replace
     * @return string
     * @throws Exception
     */
    public function insertAll($dataSet, $options = [], $replace = false)
    {
       if(is_array($options['table'])){
           $tables=explode(',',$options['table'][0]);
           // 获取合法的字段
           $insertAll=[];
           $insertFields=[];
           /*当传入的数据表为数组时执行多表批量导入*/
           foreach ($tables as $k=>$table){
               if ('*' == $options['field']) {
                   $fields = array_keys($this->query->getFieldsType($table));
               } else {
                   $fields = $options['field'];
               }
               foreach ($dataSet[$k] as $keys=> $data){
                   foreach ($data as $key => $val) {
                       //去除参数严格检查，因为TD 没有表的时候可以自动创建，严格检查会通不过
                       if (!in_array($key, $fields, true)) {
                           $data[$key]=$val;
                       } elseif (is_null($val)) {
                           $data[$key] = 'NULL';
                       } elseif (is_scalar($val)) {
                           $data[$key] = $this->parseValue($val, $key);
                       } elseif (is_object($val) && method_exists($val, '__toString')) {
                           // 对象数据写入
                           $data[$key] = $val->__toString();
                       } else {
                           // 过滤掉非标量数据
                           unset($data[$key]);
                       }
                   }
                   $value    = array_values($data);
                   $values[$k][$keys] = '('.implode(',', $value).')';

                   if (!isset($insertFields[$k])) {
                       $insertFields[$k] = array_keys($data);
                   }
               }
               /*批量替换内容*/
               $insertAll[$k]= str_replace(
                   ['%TABLE%','%SUPER%','%TAGS%', '%FIELD%', '%DATA%', '%COMMENT%'],
                   [
                       $this->parseTable($table, $options),
                       $this->parseUsing($options['using'], $options),
                       $this->parseTags($options['tags'], $options),
                       implode(' , ', $insertFields[$k]),
                        implode(' ',$values[$k]),
                       //$this->parseComment($options['comment']),
                   ], $this->insertAllTableSql);
           }
           return  str_replace(
               ['%INSERT%', '%DATA%'],
               [
                   $replace ? 'REPLACE' : 'INSERT',
                   implode(' ',$insertAll)
               ], $this->insertAllSql);
       }else{
           // 获取合法的字段
           if ('*' == $options['field']&&is_string($options['table'])) {
               $fields = array_keys($this->query->getFieldsType($options['table']));
           } else {
               $fields = $options['field'];
           }
           foreach ($dataSet as $data) {
               foreach ($data as $key => $val) {
                   if (!in_array($key, $fields, true)) {
                       $data[$key]=$val;
                   } elseif (is_null($val)) {
                       $data[$key] = 'NULL';
                   } elseif (is_scalar($val)) {
                       $data[$key] = $this->parseValue($val, $key);
                   } elseif (is_object($val) && method_exists($val, '__toString')) {
                       // 对象数据写入
                       $data[$key] = $val->__toString();
                   } else {
                       // 过滤掉非标量数据
                       unset($data[$key]);
                   }
               }
               $value    = array_values($data);
               $values[] = '('.implode(',', $value).')';

               if (!isset($insertFields)) {
                   $insertFields = array_keys($data);
               }
           }
           foreach ($insertFields as $field) {
               $fields[] = $this->parseKey($field, $options, true);
           }
           return str_replace(
               ['%INSERT%', '%TABLE%','%SUPER%','%TAGS%', '%FIELD%', '%DATA%', '%COMMENT%'],
               [
                   $replace ? 'REPLACE' : 'INSERT',
                   $this->parseTable($options['table'], $options),
                   $this->parseUsing($options['using'], $options),
                   $this->parseTags($options['tags'], $options),
                   implode(' , ', $insertFields),
                   implode(' ', $values),
                   //$this->parseComment($options['comment']),
               ], $this->insertAllSqlString);
       }
    }
    /**
     * 获取当前的Query对象实例
     * @access public
     * @return Query
     */
    public function getQuery()
    {
        return $this->query;
    }
}