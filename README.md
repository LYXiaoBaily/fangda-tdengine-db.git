TDEngineDB 1.0.0.22.9.15
===============

> * 运行环境要求PHP7.4+，兼容PHP8.1
> * TDEngine运行环境为 TDEngine server 2.6
> *  php-tdengine  https://github.com/Yurunsoft/php-tdengine.git

## 主要新特性

* 采用`PHP7`强类型（严格模式）
* 更强大和易用的查询
* 全新的事件系统
* 符合THINKPHP-ORM使用习惯
* 支持数据单个导入和批量导入
* 支持创建超级表和子表

## 安装

~~~
composer require fangda/tdengine-db
~~~


## 开发人员

人员：kimi_yang;
联系方式：kimiyangts@qq.com
## 使用方法
```php
use TDEngineOrm\TDEngine;
use TDengine\Exception\TDengineException;

## 数据库配置

TDEngine::setConfig([
    // 默认数据连接标识
    'default'     => 'TDEngine',
    // 数据库连接信息
    'connections' => [
        'TDEngine' => [
            // 数据库类型
            'type'     => 'TDEngine',
            // 主机地址
            'hostname' => '127.0.0.1',
            // 用户名
            'username' => 'root',
            // 数据库名
            'database' => 'power',
            //密码
            'password'=>'taosdata',
            //断端口
            'port'=>'6030',
            // 数据库编码默认采用utf8
            'charset'  => 'utf8'
        ],
    ]
]);
```

## 1.1新增单个数据

```php
$data['ts']=1663060608698;
$data['current']=11.0000003;
$data['voltage']=220;
$data['phase']=0.41;
$tags=['California.LosAngeles',3];
新增
$save=  TDEngine::name('power')->table('d1001')->using('meters')->tag($tags)->insert($data);
说明：name 设置数据库
          table  设置数据表
          using  设置超级表
          tags   设置TAGS
```

##  1.2 批量新增
```php
$data=[
                [
                    [
                        'ts'=>1663049549696,
                        'current'=>11.0000003,
                        'voltage'=>219,
                        'phase'=>0.44
                    ],
                    [
                        'ts'=>1663054480697,
                        'current'=>11.0000003,
                        'voltage'=>218,
                        'phase'=>0.44
                    ]
                ],
                [
                    [
                        'ts'=>1663054495696,
                        'current'=>11.0000003,
                        'voltage'=>219,
                        'phase'=>0.47
                    ]
                ]
            ];
$saveAll=  TDEngine::name('power')->table('d1001')->using('meters')->tag($tags)->insertAll($data);
说明：批量导入是table可以数组，数组的顺序和数据的插入顺序要一致，插入数据必须为3维数组.
```
##  2 创建表
```php
$Schema=[
    'ts'=>'TIMESTAMP',
    'current'=>'FLOAT',
    'voltage'=>'INT',
    'phase'=>'FLOAT'
   ];
$tags=[
    'location'=>'binary(64)',
    'groupId'=>'INT'
];
$create=TDEngine::name('power')->createTable('','fangda',$Schema,$tags,true,true);
表结构 Schema
标签   tags
参数依次传入：超级表的表名（创建子表时用到）
            要创建的表名
            表结构
            标签结构数据
            是否创建超级表
            是否开启表未创建自动创建判断
成功返回类型为空数组，创建失败返回null；

```
##表操作

> * 增加列
```php
$db=TDEngine::name('power')->table('fangda')->addColumn('names','BINARY(65)');
```
> * 删除列
```php
$db=TDEngine::name('power')->table('fangda')->dropColumn('names');
```
> * 修改列宽
```php
$db=TDEngine::name('power')->table('fangda')->modifyColumn('names','BINARY',67);
```
> * 新增标签
```php
$db=TDEngine::name('power')->table('fangda')->addTag('names','BINARY(64)');
```
> * 删除标签
```php
$db=TDEngine::name('power')->table('fangda')->dropTag('names');
```


##目前支持的查询方式
> * or查询
> * in查询
> * field查询
> * between查询
> * order查询
> * group查询
> * page查询
> * find查询

## 其他说明
其他的操作方式与think-orm使用方式一致。

